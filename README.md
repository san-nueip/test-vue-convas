# test-vue-convas

以此案[GitHub - kimuraz/vue-interact: A VueJS port for InteractJS](https://github.com/kimuraz/vue-interact)作為範本進行開發，請盡可能提供實作詳盡的commit內容。

請自行建立一個公開的repo並依照以下需求實作，完成後請提供repo網址。

## 需求
- [ ] 請以此資料源[臺中市政府消防局各單位通訊錄 - 臺中市政府消防局各單位通訊錄 - 臺中市政府資料開放平台](https://opendata.taichung.gov.tw/dataset/3ad71d4b-1a9f-11e8-8f43-00155d021202/resource/62992d6c-e0a0-4854-80fc-b38fadd31c21)任取10筆，每筆轉成一個資訊區塊(形式格式隨意)呈現於畫面上。
- [ ] 請實作前述產生的資訊區塊，可拖曳操作放入一個較大的指定區塊內。
- [ ] 請提供部署流程所需開發環境、測試環境與正式環境配置檔案，實際配置數值 測試環境可用https://testing/代表、正式環境可用https://production/代表。
